# Spring Web API and database
In this assignment I had to create a PostgreSQL database with hibernate, as well as expose it with a Web API.
The database itself is used to keep track of movies, characters and franchises, as well as their relations to each other.

To manipulate and view the database, the user can use the rest api endpoints in via their web browser or Postman.
The database contains generic CRUD, as well as some higher level getters for getting characters in a movie or franchise.

All endpoints, like their functions and usage, are documented through Swagger and spring-docs.
To access the documentation as Json, use the ```/api/docs``` endpoint. The Swagger ui can be accessed at the
```/api/docs/swagger``` endpoint.

## How to install and run
To install the database and web api locally, first clone the repository to a local folder. You then need to create
a local database that the app can use. Either create a database in pgAdmin called java-assignment-3-db, or change
the ```application.properties spring.datasource.url``` to point to a local database you would like to use.

To run the application, make sure pgAdmin is open and has access to the database you want to use. Then, run the
application through IntelliJ. You should now be able to acces the database through ```localhost:8080/api/v1/```.

All endpoints are divided into categories; characters, movies and franchises. For more information, use the Swagger
endpoint to see the documentation.

To test the application, you can use the following Postman collection:

https://www.getpostman.com/collections/8e0c0d907f769800ac5e

The database has been seeded, despite what it says in the "submit" description. Seed does not include relations
between any objects. These have to be added in post(man).

## Author
Trym Dammann Ellingsen
