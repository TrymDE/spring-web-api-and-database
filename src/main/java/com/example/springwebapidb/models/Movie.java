package com.example.springwebapidb.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private int id;
    // Semi-arbitrary number. According to a statistical analysis,
    // the max title length for a movie (for that analysis) was around 90 chars.
    @Column(length = 100, nullable = false)
    private String title;
    // Left as max, since I don't know the maximum number of genres for one movie.
    // Could potentially be a long string. Also left as nullable in case
    // genre is hard to determine or something
    private String genre;
    // Better to use int than using Date with no day or month in my opinion
    // Also left nullable in case movie release date is unknown
    private int releaseYear;
    // Same as Character name. There is an argument to leave this nullable,
    // but I feel this makes more sense
    @Column(length = 70, nullable = false)
    private String directorName;
    @Column(name = "picture_url")
    private String pictureURL;
    @Column(name = "trailer_url")
    private String trailerURL;
    // Relationships
    @ManyToMany(mappedBy = "movies")
    Set<Character> characters;
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    Franchise franchise;

    public Movie() {
    }

    public Movie(int id, String title, String genre, int releaseYear, String directorName,
                 String pictureURL, String trailerURL, Set<Character> characters,
                 Franchise franchise) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.directorName = directorName;
        this.pictureURL = pictureURL;
        this.trailerURL = trailerURL;
        this.characters = characters;
        this.franchise = franchise;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public String getTrailerURL() {
        return trailerURL;
    }

    public void setTrailerURL(String trailerURL) {
        this.trailerURL = trailerURL;
    }

    @JsonGetter("characters")
    public Set<Integer> getCharacters() {
        if (characters == null)
            return null;
        return characters.stream().map(c -> c.getId()).collect(Collectors.toSet());
    }

    public void setCharacters(Set<Character> characters) {
        this.characters = characters;
    }

    @JsonGetter("franchise")
    public Integer getFranchise() {
        if (franchise == null)
            return null;
        return franchise.getId();
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public void removeCharacter(Character character) {
        characters.remove(character);
    }
}
