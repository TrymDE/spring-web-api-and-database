package com.example.springwebapidb.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private int id;
    // Taken from (admittedly old) UK standards
    @Column(length = 70, nullable = false)
    private String name;
    @Column(length = 20)
    private String alias;
    // Chose to leave nullable in case of genderless / non-binary etc. characters
    private java.lang.Character gender;
    @Column(name = "picture_url")
    private String pictureURL;
    // Relationships
    @ManyToMany
    @JoinTable(
            name = "movie_character",
            joinColumns = {@JoinColumn(name = "character_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    Set<Movie> movies;

    public Character() {
    }

    public Character(int id, String name, String alias, java.lang.Character gender, String pictureURL, Set<Movie> movies) {
        this.id = id;
        this.name = name;
        this.alias = alias;
        this.gender = gender;
        this.pictureURL = pictureURL;
        this.movies = movies;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public java.lang.Character getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    @JsonGetter("movies")
    public Set<Integer> getMovies() {
        if (movies == null)
            return null;
        return movies.stream().map(m -> m.getId()).collect(Collectors.toSet());
    }

    /**
     * Helper method for adding a movie to character. Can't use getMovies and setMovies,
     * since getMovies returns Integers, not movies.
     * @param movie Movie to add to character
     */
    public void addMovie(Movie movie) {
        movies.add(movie);
    }

    public void removeMovie(Movie movie) {
        movies.remove(movie);
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
