package com.example.springwebapidb.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private int id;
    private String name;
    private String description;
    // relationships
    @OneToMany(mappedBy = "franchise")
    List<Movie> movies;

    public Franchise() {
    }

    public Franchise(int id, String name, String description, List<Movie> movies) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.movies = movies;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonGetter("movies")
    public List<Integer> getMovies() {
        if (movies == null)
            return null;
        return movies.stream().map(m -> m.getId()).collect(Collectors.toList());
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    /**
     * Helper method for adding movie to a franchise.
     * @param movie Movie to add to franchise.
     */
    public void addMovie(Movie movie) {
        // If there are no movies in franchise, initialize the list
        if (movies == null)
            movies = new ArrayList<>();
        // Only add movie if it doesn't exist already.
        if (!movies.contains(movie))
            movies.add(movie);
    }
}
