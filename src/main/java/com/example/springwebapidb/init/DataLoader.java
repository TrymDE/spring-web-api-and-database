package com.example.springwebapidb.init;

import com.example.springwebapidb.models.Character;
import com.example.springwebapidb.models.Franchise;
import com.example.springwebapidb.models.Movie;
import com.example.springwebapidb.repositories.CharacterRepository;
import com.example.springwebapidb.repositories.FranchiseRepository;
import com.example.springwebapidb.repositories.MovieRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;

@Component
public class DataLoader implements CommandLineRunner {

    private final CharacterRepository characterRepository;
    private final MovieRepository movieRepository;
    private final FranchiseRepository franchiseRepository;

    public DataLoader(CharacterRepository characterRepository, MovieRepository movieRepository, FranchiseRepository franchiseRepository) {
        this.characterRepository = characterRepository;
        this.movieRepository = movieRepository;
        this.franchiseRepository = franchiseRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        boolean linkData = movieRepository.count() == 0 &&
                characterRepository.count() == 0 &&
                franchiseRepository.count() == 0;
        loadCharacterData();
        loadFranchiseData();
        loadMovieData();
    }

    private void loadCharacterData() {
        if (characterRepository.count() == 0) {
            characterRepository.save(new Character(1, "Obi-Wan Kenobi", null, 'M',
                    "https://static.wikia.nocookie.net/starwars/images/9/98/Obi-wan_kenobi.JPG/revision/latest/scale-to-width-down/250?cb=20080430122037&path-prefix=no",
                    null));
            characterRepository.save(new Character(2, "Anakin Skywalker", "Darth Vader", 'M', null, null));
            characterRepository.save(new Character(3, "Ben Solo", "Kylo Ren", 'M', null, null));
            characterRepository.save(new Character(4, "Olórin", "Gandalf", 'M', null, null));
            characterRepository.save(new Character(5, "Frodo Baggins", null, 'M', null, null));
            characterRepository.save(new Character(6, "Sméagol", "Gollum", null, null, null));
            characterRepository.save(new Character(7, "Indiana Jones", "Indy", 'M', null, null));
        }
    }

    private void loadMovieData() {
        if (movieRepository.count() == 0) {
            movieRepository.save(new Movie(1, "Star Wars: Episode IV - A New Hope", "Sci-Fi, Epic, Space Opera", 1997, "George Lucas", null, null, null, null));
            movieRepository.save(new Movie(2, "Lord of The Rings: The Fellowship of the Ring", "Fantasy, Action, Adventure, Drama", 2001, "Peter Jackson", null, null, null, null));
            movieRepository.save(new Movie(3, "The Lord of the Rings: The Two Towers", "Fantasy, Action, Adventure, Drama", 2002, "Peter Jackson", null, null, null, null));
        }
    }

    private void loadFranchiseData() {
        if (franchiseRepository.count() == 0) {
            franchiseRepository.save(new Franchise(1, "The Lord of The Rings", "The lord of the rings movies, based on the writings of author J.R.R. Tolkien.", null));
            franchiseRepository.save(new Franchise(2, "Star Wars", "A sci-fi franchise depicting the battle between the Jedi order and the Sith, following the story of the Skywalker family.", null));
        }
    }

    // Couldn't get this to work. Problem with lazy loading or something.
    private void linkData() {
        Integer[] charactersInStarWarsMovie = {1, 2};
        Integer[] charactersInLOTRMovie = {4, 5, 6};
        Integer[] moviesInLOTR = {2, 3};
        Integer[] moviesInStarWars = {1};
        addCharactersToMovie(1, charactersInStarWarsMovie);
        addCharactersToMovie(2, charactersInLOTRMovie);
        addCharactersToMovie(3, charactersInLOTRMovie);
        addMoviesToFranchise(1, moviesInLOTR);
        addMoviesToFranchise(2, moviesInStarWars);
    }

    private void addCharactersToMovie(int movieId, Integer[] characterIds) {
        List<Character> characters = characterRepository.findAllById(List.of(characterIds));
        Movie movie = movieRepository.getById(movieId);
        for (Character c: characters) {
            c.addMovie(movie);
        }
        movie.setCharacters(new HashSet<>(characters));
        movieRepository.save(movie);
    }

    private void addMoviesToFranchise(int franchiseId, Integer[] movieIds) {
        List<Movie> movies = movieRepository.findAllById(List.of(movieIds));
        Franchise franchise = franchiseRepository.getById(franchiseId);
        for (Movie m: movies) {
            m.setFranchise(franchise);
            franchise.addMovie(m);
        }
        franchiseRepository.save(franchise);
    }
}
