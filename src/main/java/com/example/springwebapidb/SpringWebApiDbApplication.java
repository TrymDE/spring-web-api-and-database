package com.example.springwebapidb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebApiDbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringWebApiDbApplication.class, args);
    }

}
