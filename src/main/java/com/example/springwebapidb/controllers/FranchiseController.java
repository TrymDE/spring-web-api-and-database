package com.example.springwebapidb.controllers;

import com.example.springwebapidb.models.Character;
import com.example.springwebapidb.models.Franchise;
import com.example.springwebapidb.models.Movie;
import com.example.springwebapidb.repositories.CharacterRepository;
import com.example.springwebapidb.repositories.FranchiseRepository;
import com.example.springwebapidb.repositories.MovieRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("api/v1/franchises")
public class FranchiseController {
    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;

    public FranchiseController(FranchiseRepository franchiseRepository, MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    /**
     * Get all franchises saved in the database.
     * @return List of all franchises in database.
     */
    @GetMapping
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchiseRepository.findAll(), status);
    }

    /**
     * Get a specific franchise through an id provided in the url. Returns a 404 if no such franchise exists.
     * @param id The id of the franchise to get from database.
     * @return Either the franchise requested, or a 404.
     */
    @GetMapping("{id}")
    public ResponseEntity<Optional<Franchise>> getFranchiseById(@PathVariable int id) {
        HttpStatus status = HttpStatus.OK;
        if (!franchiseRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        return new ResponseEntity<>(franchiseRepository.findById(id), status);
    }

    /**
     * Add a franchise to the database.
     * @param franchise The franchise to add to database, formatted as a Json object.
     * @return The created franchise.
     */
    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) {
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(franchiseRepository.save(franchise), status);
    }

    /**
     * Updates an already existing franchise. Gives a 404 if no such franchise exists.
     * @param id Id of franchise to update.
     * @param franchise The updated body of the franchise.
     * @return The updated franchise or a 404 if id doesn't exist.
     */
    @PutMapping("{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable int id, @RequestBody Franchise franchise) {
        HttpStatus status = HttpStatus.OK;
        if (!franchiseRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        franchise.setId(id);
        return new ResponseEntity<>(franchiseRepository.save(franchise), status);
    }

    /**
     * Delete a franchise from the database.
     * This also means that references to said franchise is removed from all movies previously in the franchise.
     * @param id Id of the franchise to remove.
     * @return OK status.
     */
    @DeleteMapping("{id}")
    public ResponseEntity<Franchise> deleteFranchise(@PathVariable int id) {
        HttpStatus status = HttpStatus.OK;
        movieRepository.getMoviesByFranchise(franchiseRepository.getById(id)).forEach(movie -> movie.setFranchise(null));
        franchiseRepository.deleteById(id);
        return new ResponseEntity<>(status);
    }

    /**
     * Add movies to a franchise.
     * @param id Franchise to add movies to.
     * @param moviesToAdd A list of movie ids to add.
     * @return The updated franchise. 404 if the franchise doesn't exist.
     */
    @PatchMapping("{id}/update-movies")
    public ResponseEntity<Franchise> updateMoviesOfFranchise(@PathVariable int id, @RequestBody Integer[] moviesToAdd) {
        HttpStatus status = HttpStatus.OK;
        if (!franchiseRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        Franchise franchise = franchiseRepository.getById(id);
        List<Movie> moviesToUpdate = movieRepository.findAllById(Arrays.asList(moviesToAdd));
        for (Movie movie: moviesToUpdate) {
            franchise.addMovie(movie);
            movie.setFranchise(franchise);
        }
        return new ResponseEntity<>(franchiseRepository.save(franchise), status);
    }

    /**
     * Get a list of movies in the provided franchise.
     * @param id Id of the franchise to get movies of.
     * @return List of movies in specified franchise, 404 if no such franchise.
     */
    @GetMapping("/{id}/movies")
    public ResponseEntity<List<Movie>> getMoviesInFranchise(@PathVariable int id) {
        HttpStatus status = HttpStatus.OK;
        if (!franchiseRepository.existsById(id))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        List<Movie> moviesInFranchise = movieRepository.getMoviesByFranchise(franchiseRepository.getById(id));
        return new ResponseEntity<>(moviesInFranchise, status);
    }

    /**
     * Get a list of characters in the provided franchise
     * @param id Id of the franchise to get characters of.
     * @return A list of all characters in the franchise, 404 if franchise doesn't exist.
     */
    @GetMapping("{id}/characters")
    public ResponseEntity<List<Character>> getCharactersInFranchise(@PathVariable int id) {
        HttpStatus status = HttpStatus.OK;
        if (!franchiseRepository.existsById(id))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        Franchise franchise = franchiseRepository.getById(id);
        // Get movies in the franchise
        List<Movie> moviesInFranchise = movieRepository.getMoviesByFranchise(franchise);
        // Get character ids as several lists, one list per movie
        List<Integer> characterIds = moviesInFranchise.stream()
                // Combine all lists together to one list (flatten them)
                .flatMap(movie -> movie.getCharacters().stream())
                // Finally, return a list.
                .toList();
        // Add all characters with an id that is in the id list to response list.
        List<Character> charactersInFranchise = characterRepository.findAllById(characterIds);
        return new ResponseEntity<>(charactersInFranchise, status);
    }
}
