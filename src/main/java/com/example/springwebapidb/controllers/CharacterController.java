package com.example.springwebapidb.controllers;

import com.example.springwebapidb.models.Character;
import com.example.springwebapidb.repositories.CharacterRepository;
import com.example.springwebapidb.repositories.MovieRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/characters")
public class CharacterController {
    private final CharacterRepository characterRepository;
    private final MovieRepository movieRepository;

    public CharacterController(CharacterRepository characterRepository, MovieRepository movieRepository) {
        this.characterRepository = characterRepository;
        this.movieRepository = movieRepository;
    }

    /**
     * Get all characters saved in the database.
     * @return List of all characters in database.
     */
    @GetMapping
    public ResponseEntity<List<Character>> getAllCharacters() {
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characterRepository.findAll(), status);
    }

    /**
     * Get a specific character through an id provided in the url. Returns a 404 if no such character exists.
     * @param id The id of the character to get from database.
     * @return Either the character requested, or a 404.
     */
    @GetMapping("{id}")
    public ResponseEntity<Optional<Character>> getCharacterById(@PathVariable int id) {
        HttpStatus status = HttpStatus.OK;
        if (!characterRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        return new ResponseEntity<>(characterRepository.findById(id), status);
    }

    /**
     * Add a character to the database.
     * @param character The character to add to database, formatted as a Json object.
     * @return The created character.
     */
    @PostMapping
    public ResponseEntity<Character> addCharacter(@RequestBody Character character) {
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(characterRepository.save(character), status);
    }

    /**
     * Updates an already existing character. Gives a 404 if no such character exists.
     * @param id Id of character to update.
     * @param character The updated body of the character.
     * @return The updated character or a 404 if id doesn't exist.
     */
    @PutMapping("{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable int id, @RequestBody Character character) {
        HttpStatus status = HttpStatus.OK;
        if (!characterRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        character.setId(id);
        return new ResponseEntity<>(characterRepository.save(character), status);
    }

    /**
     * Delete a character from the database.
     * This also means that references to said character is removed from all movies the character is in.
     * @param id Id of the character to remove.
     * @return OK status.
     */
    @DeleteMapping("{id}")
    public ResponseEntity<Character> deleteCharacter(@PathVariable int id) {
        HttpStatus status = HttpStatus.OK;
        Character characterToDelete = characterRepository.getById(id);
        movieRepository.findAll().forEach(m -> m.removeCharacter(characterToDelete));
        characterRepository.delete(characterToDelete);
        return new ResponseEntity<>(status);
    }
}
