package com.example.springwebapidb.controllers;

import com.example.springwebapidb.models.Character;
import com.example.springwebapidb.models.Movie;
import com.example.springwebapidb.repositories.CharacterRepository;
import com.example.springwebapidb.repositories.MovieRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("api/v1/movies")
public class MovieController {
    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;

    public MovieController(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    /**
     * Get all movies saved in the database.
     * @return List of all movies in database.
     */
    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies() {
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movieRepository.findAll(), status);
    }

    /**
     * Get a specific movie through an id provided in the url. Returns a 404 if no such movie exists.
     * @param id The id of the movie to get from database.
     * @return Either the movie requested, or a 404.
     */
    @GetMapping("{id}")
    public ResponseEntity<Optional<Movie>> getMovieById(@PathVariable int id) {
        HttpStatus status = HttpStatus.OK;
        if (!movieRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        return new ResponseEntity<>(movieRepository.findById(id), status);
    }

    /**
     * Add a movie to the database.
     * @param movie The movie to add to database, formatted as a Json object.
     * @return The created movie.
     */
    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie) {
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(movieRepository.save(movie), status);
    }

    /**
     * Updates an already existing movie. Gives a 404 if no such movie exists.
     * @param id Id of movie to update.
     * @param movie The updated body of the movie.
     * @return The updated movie or a 404 if id doesn't exist.
     */
    @PutMapping("{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable int id, @RequestBody Movie movie) {
        HttpStatus status = HttpStatus.OK;
        if (!movieRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        movie.setId(id);
        return new ResponseEntity<>(movieRepository.save(movie), status);
    }

    /**
     * Delete a movie from the database.
     * This also means that references to said movie is removed from all franchises the movie is in.
     * @param id Id of the movie to remove.
     * @return OK status.
     */
    @DeleteMapping("{id}")
    public ResponseEntity<Movie> deleteMovie(@PathVariable int id) {
        HttpStatus status = HttpStatus.OK;
        Movie movieToDelete = movieRepository.getById(id);
        characterRepository.findAll().forEach(c -> c.removeMovie(movieToDelete));
        movieRepository.delete(movieToDelete);
        return new ResponseEntity<>(status);
    }

    /**
     * Add characters to a movie.
     * @param id Id of the movie to add characters to.
     * @param characterIds List of ids of the characters to add to movie.
     * @return The updated movie, or a 404 if the provided id is not a valid movie.
     */
    @PatchMapping("/{id}/update-characters")
    public ResponseEntity<Movie> UpdateCharactersOfMovie(@PathVariable int id, @RequestBody Integer[] characterIds) {
        HttpStatus status = HttpStatus.OK;
        // Check if movie exists.
        if (!movieRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        // Get the movie
        Movie movie = movieRepository.getById(id);
        // Get the characters
        List<Character> characters = characterRepository.findAllById(Arrays.asList(characterIds));
        // Add the movie to the characters
        for (Character c: characters) {
            c.addMovie(movie);
        }
        // Add the characters to the movie
        movie.setCharacters(new HashSet<>(characters));
        return new ResponseEntity<>(movieRepository.save(movie), status);
    }

    /**
     * Get all characters in a movie.
     * @param id Id of the movie to get characters of.
     * @return List of characters in movie, or 404 if movie id is not valid.
     */
    @GetMapping("{id}/characters")
    public ResponseEntity<List<Character>> getCharactersInMovie(@PathVariable int id) {
        HttpStatus status = HttpStatus.OK;
        if (!movieRepository.existsById(id))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        List<Character> charactersInMovie = characterRepository.findAllById(movieRepository.getById(id).getCharacters());
        return new ResponseEntity<>(charactersInMovie, status);
    }
}
