package com.example.springwebapidb.repositories;

import com.example.springwebapidb.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Integer> { }
