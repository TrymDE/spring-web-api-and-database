package com.example.springwebapidb.repositories;

import com.example.springwebapidb.models.Franchise;
import com.example.springwebapidb.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {
    List<Movie> getMoviesByFranchise(Franchise franchise);
}
